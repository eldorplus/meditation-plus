import { writableFields, User } from '../models/user.model.js';
import { ProfileHelper } from '../helper/profile.js';
import md5 from 'md5';
import { logger } from '../helper/logger.js';
import { getUser } from '../helper/get-user';

let ObjectId = require('mongoose').Types.ObjectId;

const getProfile = async (query, req, res) =>  {
  try {
    let doc = await getUser(query);

    if (!doc) return res.sendStatus(404);

    return res.json(doc);
  } catch (err) {
    logger.error(req.url, err);
    return res.status(400).send(err);
  }
};

export default (app, router) => {

  /**
   * @api {get} /api/profile Get profile details
   * @apiName GetProfile
   * @apiGroup Profile
   * @apiDescription Get the profile data of the currently logged in user.
   *
   * @apiSuccess {Object}     local           Details for local authentication
   * @apiSuccess {String}     local.email     Email address
   * @apiSuccess {String}     name            Name
   * @apiSuccess {Boolean}    showEmail       Show email publicly
   * @apiSuccess {String}     description     Profile description
   * @apiSuccess {String}     website         Website
   * @apiSuccess {String}     country         Country
   * @apiSuccess {String}     gravatarHash    Hash for Gravatar
   */
  router.get('/api/profile', async (req, res) => {
    try {
      let doc = await User
        .findOne({
          _id: req.user._id
        })
        .lean()
        .exec();

      delete doc.local['password'];
      delete doc['__v'];

      res.json(doc);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {get} /api/profile/:id Get profile details of a user
   * @apiName ShowProfile
   * @apiGroup Profile
   * @apiDescription Get the profile data of a user.
   *
   * @apiSuccess {Object}     local             Details for local authentication
   * @apiSuccess {String}     local.email       Email address (if public)
   * @apiSuccess {String}     name              Name
   * @apiSuccess {String}     description       Profile description
   * @apiSuccess {String}     website           Website
   * @apiSuccess {String}     country           Country
   * @apiSuccess {Object}     meditations       Last week meditation times
   * @apiSuccess {String}     gravatarHash      Hash for Gravatar
   */
  router.get('/api/profile/:id', async (req, res) => {
    return getProfile({ '_id': ObjectId(req.params.id) }, req, res);
  });

  /**
   * @api {get} /api/profile/username/:username Get profile details of a user by username
   * @apiName ShowProfileByUsername
   * @apiGroup Profile
   * @apiDescription Get the profile data of a user.
   *
   * @apiSuccess {Object}     local             Details for local authentication
   * @apiSuccess {String}     local.email       Email address (if public)
   * @apiSuccess {String}     name              Name
   * @apiSuccess {String}     description       Profile description
   * @apiSuccess {String}     website           Website
   * @apiSuccess {String}     country           Country
   * @apiSuccess {Object}     meditations       Last week meditation times
   * @apiSuccess {String}     gravatarHash      Hash for Gravatar
   */
  router.get('/api/profile/username/:username', async (req, res) => {
    return getProfile({ 'username': req.params.username }, req, res);
  });

  /**
   * @api {get} /api/profile/stats/:usernameOrId Get profile statistics of a user by username or id
   * @apiName ShowStats
   * @apiGroup Profile
   * @apiDescription Get the profile statistics of a user.
   *
   * @apiSuccess {Object}     general           Generals meditation stats
   * @apiSuccess {Object}     chartData         Historical meditation stats for
   *                                            three different chart types
   * @apiSuccess {Object}     consecutiveDays   Number of current and total consecutive
   *                                            days of meditation
   */
  router.get('/api/profile/stats/:usernameOrId/:monthOffset?', async (req, res) => {
    try {
      const user = await User.findOne({
        $or: [
          { _id: ObjectId.isValid(req.params.usernameOrId) ? ObjectId(req.params.usernameOrId) : null },
          { username: req.params.usernameOrId }
        ]
      });

      if (user && user.hideStats && user._id.toString() !== req.user._id &&
        req.user.role !== 'ROLE_ADMIN') {
        return res.sendStatus(403);
      }

      if (!user) {
        return res.sendStatus(404);
      }

      const pHelper = new ProfileHelper(user);

      const monthOffset = req.params.monthOffset ? parseInt(req.params.monthOffset, 10) : -1;
      if (!isNaN(monthOffset) && monthOffset >= 0) {
        // only return month data if offset specified
        return res.json(await pHelper.getMonthChartData(monthOffset));
      }

      const result = {
        general: await pHelper.getGeneralStats(user),
        chartData: await pHelper.getChartData(user),
        consecutiveDays: await pHelper.getConsecutiveDays(user)
      };

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {put} /api/profile Update profile
   * @apiName UpdateProfile
   * @apiGroup Profile
   * @apiDescription Updating the profile of the currently logged in user.
   *
   * @apiParam {Boolean}    showEmail       Show email publicly
   * @apiParam {String}     description     Profile description
   * @apiParam {String}     website         Website
   * @apiParam {String}     country         Country
   * @apiParam {String}     gravatarHash    Hash for Gravatar
   */
  router.put('/api/profile', async (req, res) => {

    if (!req.body.local) {
      return res.status(400).send('Missing local field');
    }

    try {
      let user = await User.findById(req.user._id);

      // change password if set
      if (req.body.newPassword && req.body.newPassword.length >= 8
        && req.body.newPassword.length <= 128) {
        user.local.password = user.generateHash(req.body.newPassword);
        delete req.body.newPassword;
      }

      for (const key of Object.keys(req.body)) {
        if (key === 'local' && req.body.local.email) {
          user.local.email = req.body.local.email;
          continue;
        }

        if (key === 'isTeacher' && req.user.role === 'ROLE_ADMIN') {
          user.isTeacher = req.body.isTeacher;
          continue;
        }

        if (!writableFields.includes(key)) {
          continue;
        }

        user[key] = req.body[key];
      }

      if (user.local && user.local.email) {
        user.gravatarHash = md5(user.local.email);
      }

      await user.save();

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {delete} /api/profile Delete account
   * @apiName DeleteProfile
   * @apiGroup Profile
   * @apiDescription Deleting the profile of the currently logged in user.
   */
  router.delete('/api/profile', async (req, res) => {
    try {
      const user = await User.findById(req.user._id);
      await user.remove();
      req.logout();
      req.session = null;
      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};
