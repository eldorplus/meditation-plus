import AppointmentMeeting from '../models/meeting.model.js';
import { User } from '../models/user.model.js';
import FCMSubscription from '../models/fcm.model.js';
import { logger } from '../helper/logger.js';
import appointHelper from '../helper/appointment.js';
import moment from 'moment';
import randomstring from 'randomstring';
import firebase from 'firebase-admin';

export default (app, router, io, admin) => {
  /**
   * @api {get} /api/meeting  Find an appointment meeting for right now or automatically create one if possible.
   * @apiName GetMeeting
   * @apiGroup Meeting
   */
  router.get('/api/meeting', async (req, res) => {
    try {
      // try to find current meeting first
      const meeting = await AppointmentMeeting
        .findOne(
          { createdAt: { $gt: moment().subtract(20, 'minutes') }, closed: { $ne: true } }, '',
          { sort: { 'created_at' : 1 } }
        )
        .populate('user', '_id name username gravatarHash');

      if (meeting) {
        // check authorization
        if (req.user.username !== 'yuttadhammo' && meeting.user._id.toString() !== req.user._id.toString()) {
          return res.sendStatus(403);
        }

        if (req.user.username === 'yuttadhammo' && !meeting.initiated) {
          return res.sendStatus(400);
        }

        return res.json(meeting);
      }

      // try to create new meeting by looking for appointments the user has right now
      const appointment = await appointHelper.getNow(req.user);
      if (appointment) {
        const newMeeting = await AppointmentMeeting.create({
          user: req.user._id,
          initiated: false,
          closed: false,
          token: appointment.hour.toString() + '-' + req.user.username + '-' + randomstring.generate(8)
        });

        return res.json(newMeeting);
      }

      return res.sendStatus(403);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send('Internal Server Error');
    }
  });

  /**
   * @api {get} /api/meeting/initiate  Initiate an existing appointment. This will trigger the call.
   * @apiName InitiateMeeting
   * @apiGroup Meeting
   */
  router.post('/api/meeting/initiate', async (req, res) => {
    try {
      const meeting = await AppointmentMeeting
        .findOne({
          user: req.user._id,
          initiated: false,
          closed: { $ne: true },
          createdAt: { $gte: moment().subtract(20, 'minutes') }
        })
        .populate('user', 'username name');

      if (!meeting) {
        return res.sendStatus(403);
      }

      await meeting.update({ initiated: true });

      // Send out call notification
      const teacher = await User.findOne({ username: 'yuttadhammo' });

      if (!teacher) {
        return res.json({ callError: true });
      }

      const subscriptions = await FCMSubscription
        .find({ user: teacher._id })
        .lean();

      for (const sub of subscriptions) {
        try {
          await firebase
            .messaging()
            .send({
              token: sub.token,
              android: {
                priority: 'high'
              },
              data: {
                roomId: meeting.token
              }
            });
        } catch (fcmErr) {
          logger.error(req.url, fcmErr.toJSON());
        }
      }

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send('Internal Server Error');
    }
  });

  /**
   * @api {get} /api/meeting/close  Close an open meeting.
   * @apiName CloseMeeting
   * @apiGroup Meeting
   */
  router.post('/api/meeting/close', admin, async (req, res) => {
    try {
      const meetingId = req.body.meetingId ? req.body.meetingId : '';

      if (!meetingId) {
        return res.sendStatus(400);
      }


      const meeting = await AppointmentMeeting.findOne({ _id: meetingId });
      if (!meeting) {
        return res.sendStatus(400);
      }

      await meeting.update({ closed: true });

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send('Internal Server Error');
    }
  });
};
