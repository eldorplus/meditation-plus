import mongoose from 'mongoose';

let videoSuggestionSchema = mongoose.Schema({
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  question: { type: mongoose.Schema.Types.ObjectId, ref: 'Question' },
  videoUrl: String
}, {
  timestamps: true
});

export default mongoose.model('VideoSuggestion', videoSuggestionSchema);
