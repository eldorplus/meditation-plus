Welcome {{userName}},

In order to activate your new account, please verify your email address by clicking on the link below.

{{activationLink}}

If you have problems logging in or experience other technical issues, please send an email to it@sirimangalo.org.
