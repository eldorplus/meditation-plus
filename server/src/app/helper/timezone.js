import moment from 'moment-timezone';
const timezones = require('timezones.json');

/**
* Method for converting times to user's timezone
*/
export default (user, time) => {
  if (!user.timezone) {
    return moment(time).utc();
  }

  let detectedTz = null;

  for (const timezone of timezones) {
    if (timezone.value === user.timezone) {
      detectedTz = timezone;
      break;
    }
  }

  if (!detectedTz || detectedTz < 0) {
    return moment(time).utc();
  }

  return moment(time).utc().utcOffset(detectedTz.offset);
};
