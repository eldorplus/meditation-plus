import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppointmentComponent } from './appointment.component';
import { MaterialModule } from '../shared/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../user/user.service';
import { FakeUserService } from '../user/testing/fake-user.service';
import { AppointmentService } from './appointment.service';
import { FakeAppointmentService } from './testing/fake-appointment.service';
import { SettingsService } from '../shared/settings.service';
import { FakeSettingsService } from '../shared/testing/fake-settings.service';
import { AvatarDirective } from '../profile/avatar.directive';
import { SharedModule } from '../shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';

describe('AppointmentComponent', () => {
  let component: AppointmentComponent;
  let fixture: ComponentFixture<AppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        SharedModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppointmentComponent,
        AvatarDirective
      ],
      providers: [
        {provide: UserService, useClass: FakeUserService},
        {provide: AppointmentService, useClass: FakeAppointmentService},
        {provide: SettingsService, useClass: FakeSettingsService},
        {provide: Store, useClass: MockStore}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = <ComponentFixture<AppointmentComponent>>TestBed.createComponent(AppointmentComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();  // call ngInit
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

