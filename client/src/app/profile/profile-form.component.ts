import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { ProfileDeleteDialogComponent } from './profile-delete-dialog.component';
import { filter, concatMap } from 'rxjs/operators';
import { AppState } from '../reducers';
import { SetTitle, ThrowError } from '../actions/global.actions';
import { Store } from '@ngrx/store';
import { Logout } from '../auth/actions/auth.actions';
import { DialogService } from '../dialog/dialog.service';

@Component({
  selector: 'profile-form',
  templateUrl: './profile-form.component.html'
})
export class ProfileFormComponent implements OnInit {

  profile;
  loading: boolean;
  error: boolean;

  constructor(
    private userService: UserService,
    private dialog: MatDialog,
    private store: Store<AppState>,
    private dialogService: DialogService,
    private snackbar: MatSnackBar
  ) {
    this.store.dispatch(new SetTitle('Your profile'));
  }

  save() {
    // check if passwords equal
    if (this.profile.newPassword &&
      this.profile.newPassword !== this.profile.newPasswordRepeat) {
      this.dialogService.alert('Error', 'Passwords do not match.');
      return;
    }

    // remove repeated password from payload
    if (this.profile.newPassword) {
      delete this.profile.newPasswordRepeat;
    }

    this.loading = true;
    this.userService.updateProfile(this.profile)
      .subscribe(
        () => this.snackbar.open('Your profile has been updated.'),
        err => {
          this.loading = false;
          this.store.dispatch(new ThrowError({
            message: 'There was an error saving your profile.',
            error: err
          }));
        },
        () => this.loading = false
      );
  }

  deleteAccount() {
    let dialogRef: MatDialogRef<ProfileDeleteDialogComponent>;

    dialogRef = this.dialog.open(ProfileDeleteDialogComponent);

    dialogRef.afterClosed()
      .pipe(
        filter(res => !!res),
        concatMap(() => this.userService.deleteProfile())
      )
      .subscribe(() => this.store.dispatch(new Logout()));
  }

  ngOnInit() {
    this.userService.getProfile()
      .subscribe(
        data => this.profile = data,
        err => console.error(err)
      );
  }
}
