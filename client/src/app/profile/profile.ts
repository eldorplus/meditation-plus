import { Moment } from 'moment';

export interface Profile {
  _id: string;
  createdAt: Date;
  username: string;
  suspendedUntil?: Date;
  name: string;
  role: string;
  showEmail: boolean;
  hideStats: boolean;
  description: string;
  website: string;
  country: string;
  lastLike: Moment | null;
  sound: string;
  stableBell: boolean;
  gravatarHash: string;
  timezone: string;
  verified: boolean;
  acceptedGdpr: boolean;
  acceptedGdprDate: Date;
  recoverUntil: Date;
  notifications: {
    livestream: boolean,
    message: boolean,
    meditation: boolean,
    question: boolean,
    testimonial: boolean,
    appointment: any
  };
  meetings: any;
  isTeacher: boolean;
}
