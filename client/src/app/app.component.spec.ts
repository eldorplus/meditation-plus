import { MaterialModule } from './shared/material.module';
import { TestBed, async } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent } from 'ng2-mock-component';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockComponent({selector: 'online', inputs: ['detailed']}),
      ],
      imports: [
        MaterialModule,
        HttpClientModule,
        NoopAnimationsModule,
        RouterTestingModule,
        SharedModule
      ],
      providers: [
        {provide: Store, useClass: MockStore}
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
