import { Action } from '@ngrx/store';

export const SET_TITLE = '[Global] Set Title';
export const SET_SIDENAV_OPEN = '[Global] Set Sidenav Open';
export const SET_IS_MEDITATING = '[Global] Set Is Meditating';
export const SET_CONNECTION_STATUS = '[Global] Set Connection Status';
export const THROW_ERROR = '[Global] Throw Error';

export class SetTitle implements Action {
  readonly type = SET_TITLE;
  constructor(public payload: string) {}
}

export interface ThrowErrorPayload {
  message?: string;
  error?: any;
}
export class ThrowError implements Action {
  readonly type = THROW_ERROR;
  constructor(public payload: ThrowErrorPayload) {}
}


export class SetSidenavOpen implements Action {
  readonly type = SET_SIDENAV_OPEN;
  constructor(public payload: boolean) {}
}

export class SetConnectionStatus implements Action {
  readonly type = SET_CONNECTION_STATUS;
  constructor(public payload: number) {}
}

export type Actions = SetTitle | SetSidenavOpen | SetConnectionStatus
;
