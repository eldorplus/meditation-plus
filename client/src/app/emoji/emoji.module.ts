import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmojiSelectComponent } from './emoji-select.component';
import { EmojiPipe } from './emoji.pipe';
import { MaterialModule } from 'app/shared/material.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    EmojiSelectComponent,
    EmojiPipe
  ],
  exports: [
    EmojiSelectComponent,
    EmojiPipe
  ]
})
export class EmojiModule { }
