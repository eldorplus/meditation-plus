import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectToken } from '../auth/reducders/auth.reducers';
import { take, tap, concatMap } from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  public constructor(private store: Store<AppState>) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent
    | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    return this.store.select(selectToken).pipe(
      take(1),
      tap(() => {
        if (req.method === 'POST' || req.method === 'PUT') {
          // add content type to post and put requests
          req = req.clone({
            setHeaders: {
              'Content-Type': 'application/json'
            }
          });
        }
      }),
      tap(token => {
        // add Authorization header when token is set
        if (token) {
          req = req.clone({
            setHeaders: {
              'Authorization': 'Bearer ' + token
            }
          });
        }
      }),
      concatMap(() => next.handle(req))
    );
  }
}
