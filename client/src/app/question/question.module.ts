import { UserTextListModule } from './../user-text-list/user-text-list.module';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared';
import { QuestionComponent } from './question.component';
import { QuestionListEntryComponent } from './list-entry/question-list-entry.component';
import { QuestionSuggestionsComponent } from './suggestions/suggestions.component';
import { EmojiModule } from '../emoji';
import { ProfileModule } from '../profile';
import { MomentModule } from 'angular2-moment';
import { BroadcastUrlPipe } from './list-entry/broadcast-url.pipe';
import { SuggestVideoUrlDialogComponent } from './suggest-videourl-dialog/suggest-videourl-dialog.component';
import { EffectsModule } from '@ngrx/effects';
import { QuestionEffect } from './effects/question.effect';
import { HasTimecodePipe } from './suggest-videourl-dialog/has-timecode.pipe';

@NgModule({
  imports: [
    SharedModule,
    MomentModule,
    ProfileModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    EmojiModule,
    UserTextListModule,
    EffectsModule.forFeature([
      QuestionEffect
    ])
  ],
  entryComponents: [SuggestVideoUrlDialogComponent],
  declarations: [
    QuestionComponent,
    QuestionListEntryComponent,
    QuestionSuggestionsComponent,
    BroadcastUrlPipe,
    SuggestVideoUrlDialogComponent,
    HasTimecodePipe
  ],
  exports: [
    QuestionComponent,
    QuestionListEntryComponent,
    QuestionSuggestionsComponent
  ]
})
export class QuestionModule { }
