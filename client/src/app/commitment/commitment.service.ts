import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CommitmentService {

  public constructor(private http: HttpClient) {
  }

  public getAll(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/commitment');
  }

  public get(id: string): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/commitment/' + id);
  }

  public getCurrentUser(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/commitment/user');
  }

  public save(commitment): Observable<any> {
    if (commitment._id) {
      // edit
      return this.http.put(
        ApiConfig.url + '/api/commitment/' + commitment._id,
        commitment
      );
    }

    return this.http.post(
      ApiConfig.url + '/api/commitment',
      commitment,
      { observe: 'response', responseType: 'text' }
    );
  }

  public commit(commitment): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/commitment/' + commitment._id + '/commit', '');
  }

  public uncommit(commitment): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/commitment/' + commitment._id + '/uncommit', '');
  }

  public delete(commitment): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/commitment/' + commitment._id);
  }

  /**
   * Determines the percentage of the reached goal.
   */
  public reached(meditations, commitment): number {
    if (!meditations || !commitment) {
      return;
    }

    if (commitment.type === 'daily') {
      let sum = 0;

      // Sum minutes per day for the last week
      for (const key of Object.keys(meditations.lastDays)) {
        const meditated = meditations.lastDays[key];
        // Cut meditated minutes to the max of the commitment to preserve
        // a correct average value.
        sum += meditated > commitment.minutes ? commitment.minutes : meditated;
      }

      // build the average and compare it to goal
      const avg = sum / Object.keys(meditations.lastDays).length;
      const result = Math.round(100 * avg / commitment.minutes);

      return result;
    }

    if (commitment.type === 'weekly') {
      const keys = Object.keys(meditations.lastWeeks);

      // Get last entry of lastWeeks
      const lastWeekSum = meditations.lastWeeks[keys[keys.length - 1]];

      // compare it to goal
      const result = Math.round(100 * lastWeekSum / commitment.minutes);

      return result;
    }
  }

}
