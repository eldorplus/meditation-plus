import { Component } from '@angular/core';
import { DialogService } from '../dialog/dialog.service';
import { SettingsService } from '../shared';
import { concatMap, tap, filter, map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { ThrowError } from '../actions/global.actions';

@Component({
  selector: 'admin-index',
  templateUrl: './admin-index.component.html'
})
export class AdminIndexComponent {
  loadingNotification = false;

  constructor(
    private dialog: DialogService,
    private settings: SettingsService,
    private snackbar: MatSnackBar,
    private store: Store<AppState>
  ) {
  }

  inputNotification() {
    this.loadingNotification = true;
    this.settings.get().pipe(
      tap(() => this.loadingNotification = false),
      map(settings => settings.notification),
      concatMap(currentText => this.dialog.input(
        'Notification banner',
        `Set the text for the notification banner users will
        see on the homescreen of the app.`,
        'Notification text',
        currentText
      )),
      filter(val => typeof val !== 'undefined'),
      concatMap(val => this.settings.set('notification', val)),
    ).subscribe(
      () => this.snackbar.open('Banner text has been set successfully.'),
      error => this.store.dispatch(new ThrowError({ error }))
    );
  }
}
