import { Component, ChangeDetectorRef } from '@angular/core';
import { TestimonialService } from '../../testimonial';
import { DialogService } from '../../dialog/dialog.service';
import { filter, concatMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'testimonial-admin',
  templateUrl: './testimonial-admin.component.html',
  styleUrls: [
    './testimonial-admin.component.styl'
  ]
})
export class TestimonialAdminComponent {

  // testimonial data
  testimonials: any[];
  showOnlyUnreviewed = true;

  constructor(
    private testimonialService: TestimonialService,
    private dialog: DialogService,
    private chDet: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private store: Store<AppState>
  ) {
    this.loadTestimonials();
  }

  /**
   * Loads all testimonials
   */
  loadTestimonials() {
    this.testimonialService.getAllAdmin()
      .subscribe(data => {
        this.testimonials = data.testimonials;
        this.chDet.detectChanges();
      });
  }

  delete(evt, testimonial) {
    evt.preventDefault();

    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      concatMap(() => this.testimonialService.delete(testimonial))
    ).subscribe(
      () => {
        this.loadTestimonials();
        this.snackbar.open('The testimonial has been deleted successfully.');
      },
      error => this.store.dispatch(new ThrowError(error))
    );
  }

  toggleReviewed(testimonial) {
    this.testimonialService
      .toggleReviewed(testimonial._id)
      .subscribe(
        () => {
          this.loadTestimonials();
          this.snackbar.open('The testimonial has been updated successfully.');
        },
        error => this.store.dispatch(new ThrowError(error))
      );
  }

}
