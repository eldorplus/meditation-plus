import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommitmentService } from '../../commitment';
import { MatSnackBar } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'commitment-form',
  templateUrl: './commitment-form.component.html',
  styleUrls: [
    './commitment-form.component.styl'
  ]
})
export class CommitmentFormComponent {

  commitment;
  loading = false;

  constructor(
    private commitmentService: CommitmentService,
    private router: Router,
    private snackbar: MatSnackBar,
    private store: Store<AppState>,
    route: ActivatedRoute
  ) {
    this.commitment = {};

    if (route.snapshot.params['id']) {
      this.commitmentService
        .get(route.snapshot.params['id'])
        .subscribe(res => this.commitment = res);
    }
  }

  submit() {
    this.loading = true;
    this.commitmentService
      .save(this.commitment)
      .subscribe(
        () => {
          this.snackbar.open('Commitment has been saved successfully.');
          this.router.navigate(['/admin/commitments']);
        },
        error => this.store.dispatch(new ThrowError({ error })),
        () => this.loading = false
      );
  }
}
