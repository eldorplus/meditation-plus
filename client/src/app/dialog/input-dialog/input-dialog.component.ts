import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-input-dialog',
  templateUrl: './input-dialog.component.html',
  styleUrls: ['./input-dialog.component.styl']
})
export class InputDialogComponent {
  input: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<InputDialogComponent>
  ) {
    this.input = data.initialData;
  }

  submit() {
    this.dialogRef.close(this.input);
  }

}
