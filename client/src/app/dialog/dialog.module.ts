import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { InputDialogComponent } from './input-dialog/input-dialog.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material.module';

export const DIALOGS = [
  AlertDialogComponent, ConfirmDialogComponent, InputDialogComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  declarations: DIALOGS,
  exports: DIALOGS,
})
export class DialogModule { }
