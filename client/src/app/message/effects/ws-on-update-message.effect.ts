import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { MessageService } from 'app/message/message.service';
import { WebsocketOnUpdateMessage } from 'app/message/actions/message.actions';
import { of } from 'rxjs';
import { concatMap } from 'rxjs/operators';

@Injectable()
export class WsOnUpdateMessageEffect {
  constructor(
    private service: MessageService
  ) {
  }

  @Effect()
  wsOnConnect$ = this.service.getUpdateSocket()
    .pipe(
      concatMap(data => of(new WebsocketOnUpdateMessage(data.populated)))
    );
}
