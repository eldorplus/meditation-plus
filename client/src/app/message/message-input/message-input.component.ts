import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { AutocompleteMessageEffect } from '../effects/autocomplete-message.effect';
import { selectPosting } from '../reducers/message.reducers';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { PostMessage, AutocompleteUser, POST_DONE, PostMessageDone } from '../actions/message.actions';
import { AppState } from '../../reducers';
import { Actions } from '@ngrx/effects';
import { MatBottomSheet } from '../../../../node_modules/@angular/material';
import { EmojiSelectComponent } from '../../emoji';

@Component({
  selector: 'message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.styl'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageInputComponent {
  form: FormGroup;
  message: FormControl;

  posting$: Observable<boolean>;

  constructor(
    private store: Store<AppState>,
    private sheet: MatBottomSheet,
    autocompleteEffect: AutocompleteMessageEffect,
    actions$: Actions
  ) {
    this.message = new FormControl();
    this.form = new FormGroup({
      message: this.message
    });

    this.posting$ = store.select(selectPosting);

    this.posting$.subscribe(val => val
      ? this.message.disable()
      : this.message.enable()
    );
    autocompleteEffect.$autocomplete
      .pipe(filter(val => !!val))
      .subscribe(val => this.message.setValue(val));

    actions$.ofType<PostMessageDone>(POST_DONE)
      .subscribe(() => this.message.setValue(''));
   }

  showEmojiSelect() {
    this.sheet.open(EmojiSelectComponent).afterDismissed()
      .pipe(filter(val => !!val))
      .subscribe(val => this.message.setValue(`${this.message.value || ''}:${val}:`));
  }

  sendMessage(evt: KeyboardEvent) {
    evt.preventDefault();
    if (!this.message.value.trim()) {
      return;
    }
    this.store.dispatch(new PostMessage(this.message.value));
  }

  autocomplete(evt: KeyboardEvent) {
    evt.preventDefault();
    this.store.dispatch(new AutocompleteUser({
      cursorPosition: (evt.target as HTMLTextAreaElement).selectionEnd
        ? (evt.target as HTMLTextAreaElement).selectionEnd
        : this.message.value.length,
      message: this.message.value
    }));
  }

}
