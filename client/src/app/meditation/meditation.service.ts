import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { WebsocketService } from '../shared';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';
import { Meditation } from 'app/meditation/meditation';

@Injectable()
export class MeditationService {

  public constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) {
  }

  public getRecent(): Observable<Meditation[]> {
    return this.http.get<Meditation[]>(ApiConfig.url + '/api/meditation');
  }

  public getChartData(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/meditation/chart');
  }

  public post(walking: number, sitting: number, start = null): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/meditation',
      JSON.stringify({ walking, sitting, start })
    );
  }

  public stop(): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/meditation/stop', '');
  }

  public like(): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/meditation/like', '');
  }

  /**
   * Initializes Socket.io client with Jwt and listens to 'meditation'.
   */
  public getSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('meditation', res => obs.next(res));
      }))
    );
  }
}
