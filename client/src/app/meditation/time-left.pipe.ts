import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timeLeft',
  pure: false
})
export class TimeLeftPipe implements PipeTransform {

  transform(value: any, args?: any, seconds = false): any {
    const start = moment(value.createdAt);
    const end = moment(value.end);

    if (args === 'walking') {
      if (value.walking === 0) {
        return 0;
      }

      end.subtract(value.sitting, 'minutes');
    }

    if (args === 'sitting') {
      start.add(value.walking, 'minutes');
    }

    const diff = moment.duration(end.diff(start));
    const done = moment.duration(end.diff(moment()));

    if (diff.asSeconds() === 0) {
      return 0;
    }

    if (done.asMinutes() > value[args]) {
      return seconds
        ? moment.utc(moment.duration(value[args], 'minutes').asMilliseconds()).format(
          (value[args] > 60 ? 'hh:' : '') + 'mm:ss'
        )
        : 100;
    }
    if (done.asSeconds() < 0) {
      return seconds
        ? '00:00'
        : 0;
    }

    return seconds
      ? moment.utc(done.as('milliseconds')).format(
        (done.asMinutes() > 60 ? 'hh:' : '') + 'mm:ss'
      )
      : 100 * done.asSeconds() / diff.asSeconds();
  }

}
