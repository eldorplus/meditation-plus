import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Observable, interval } from 'rxjs';
import { Meditation } from 'app/meditation/meditation';
import * as meditation from '../reducers/meditation.reducers';
import { AppState } from 'app/reducers';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { CancelMeditation } from 'app/meditation/actions/meditation.actions';
import { DialogService } from 'app/dialog/dialog.service';

@Component({
  selector: 'app-own-meditation',
  templateUrl: './own-meditation.component.html',
  styleUrls: ['./own-meditation.component.styl'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OwnMeditationComponent {

  ownSession$: Observable<Meditation | null>;
  activeMeditations$: Observable<Meditation[]>;

  constructor(
    private store: Store<AppState>,
    private dialog: DialogService,
    chDet: ChangeDetectorRef
  ) {
    this.activeMeditations$ = this.store.select(meditation.selectActive);
    this.ownSession$ = this.store.select(meditation.selectOwnSession);

    interval(500).subscribe(() => chDet.markForCheck());
  }

  /**
   * Stopping active meditation session.
   */
  stop() {
    this.dialog.confirm(
      'Stop session?',
      'Are you sure you want to stop your session?'
    ).pipe(filter(val => !!val)).subscribe(() => this.store.dispatch(new CancelMeditation()));
  }

  /**
   * Rounds a number. Math.round isn't available in the template.
   * Needed for meditation progress.
   *
   * @param  {number} val Value to round
   * @return {number}     Rounded value
   */
  round(val: number): number {
    return Math.round(val);
  }
}
